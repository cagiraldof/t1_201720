package model.logic;

import java.util.Iterator;

import model.data_structures.NumbersBag;

public class NumbersBagOperations <T extends Number>{

	
	
	public Number computeMean(NumbersBag<T> bag){
		Number mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<T> iter = bag.getIterator();
			while(iter.hasNext()){
				mean = mean.doubleValue()+iter.next().doubleValue();
				length++;
			}
			if( length > 0) mean = mean.doubleValue() / length;
		}
		return mean;
	}
	
	
	public Number getMax(NumbersBag <T> bag){
		Number max = Double.MIN_VALUE;
	    T value;
		if(bag != null){
			Iterator<T> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max.doubleValue() < value.doubleValue()){
					max = value;
				}
			}
			
		}
		return max;
	}
}
