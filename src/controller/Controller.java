package controller;

import java.util.ArrayList;

import model.data_structures.NumbersBag;
import model.logic.NumbersBagOperations;

public class Controller{

	private static NumbersBagOperations model = new NumbersBagOperations();
	
	
	public static NumbersBag  createBag(ArrayList values){
         return new NumbersBag(values);		
	}
	
	
	public static Number getMean(NumbersBag bag){
		return model.computeMean(bag);
	}
	
	public static Number getMax(NumbersBag bag){
		return model.getMax(bag);
	}
	
}
